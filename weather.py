import requests
import datetime

def get_weather(city, key):
    try:
        api_url = 'https://api.openweathermap.org/data/2.5/weather'
        res = requests.get(api_url,
                           params = {'q' : city, 'appid' : key, 'lang' : 'RU'})
        res = res.json()
        ans = {}
        ans['current_temp'] = round(res['main']['temp'] - 273, 1)
        ans['max_temp'] = round(res['main']['temp_max'] - 273, 1)
        ans['min_temp'] = round(res['main']['temp_min'] - 273, 1)
        ans['humidity'] = res['main']['humidity']
        ans['w_desc'] = res['weather'][0]['description']
        return ans
    except:
        return 'Невозможно узнать погоду'


def get_forecast(city, key):
    try:
        api_url= 'https://api.openweathermap.org/data/2.5/forecast'
        res = requests.get(api_url,
                           params = {'q' : city, 'appid' : key, 'lang' : 'RU'})
        res = res.json()
        forecast = []
        for i in res['list']:
            point_weather = {}
            point_weather['current_temp'] = round(i['main']['temp'] - 273, 1)
            point_weather['max_temp'] = round(i['main']['temp_max'] - 273, 1)
            point_weather['min_temp'] = round(i['main']['temp_min'] - 273, 1)
            point_weather['humidity'] = i['main']['humidity']
            point_weather['w_desc'] = i['weather'][0]['description']
            point_weather['timestamp'] = datetime.datetime.fromtimestamp( i['dt'] )
            forecast.append(point_weather)
        return forecast
    except:
        return 'Невозможно узнать прогноз'

    
key = '5fee3768a934f574254b78789b84225d'


